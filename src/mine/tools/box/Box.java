package mine.tools.box;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import mine.typed.GL.GLGame;
import mine.typed.core.interfaces.FileIO;


/**
 * Box 는 리소스를 압축하여 관리 하는 용도로 만들어진 리소스 관리 체제 입니다.<P>
 * 사용하려는 리소스 들을 압축 하여 assets 폴더에 넣은 후, Texture 생성 하듯 객체를 생성 하면 됩니다.<P>
 * 초기화가 시작 되면 객체는 파일의 압축을 풀고 메모리에 리소스를 등록 합니다.<P>
 * 현재 사용중인 방법은 그러합니다.<P>
 * 차후 조금의 실험을 통해 압축을 캐쉬 공간에 풀어두고 해당 리소스 팩이 캐시지역에 압축이 풀렸는지에 대한 여부를 확인하여
 * 리소스를 사용하는 방법을 사용할지 말지 결정 하겠습니다.<P>
 * 
 * 개인적으로는 후자가 더 좋은 방법 인듯 합니다만 결과는 실험후에 알겠죠.<P>
 * @author mrminer
 *
 */
public class Box {

	/**
	 * 읽어들인 파일들
	 */
	public List<Object> objects;

	FileIO fileIO;
	String fileName;

	/**
	 * Box 객체를 생성 합니다.
	 * @param glGame
	 * @param fileName
	 */
	public Box(final GLGame glGame, final String fileName){
		this.fileIO = glGame.getFileIO( );
		this.fileName = fileName;
	}

	/**
	 * -!!-미완성 된 코드 입니다! 사용하지 마세요-!!-<p>
	 *	fileName 을 사용하여 Assets 에서 Box 를 찾습니다.
	 * 그후 Box 를 UnPacked 하여 메모리에 올린후 Box 의 파일들의 확장자를 조회 합니다.
	 * <P>
	 * <code>.png = Texture</code>
	 * <P>
	 * <code>.hid = Hiden Item Data</code>
	 * <P>
	 * <code>.ogg , .mp3 = TypeSound</code>
	 * <P>
	 */
	public void loadBox(){
		final List<Object> tmp = null;

		InputStream in = null;
		try {
			in = this.fileIO.readAsset( this.fileName );
			in.close( );
		} catch ( final IOException ee ) {
			ee.printStackTrace();
		}

		this.objects = tmp;

	}

}
