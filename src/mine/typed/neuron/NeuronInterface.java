package mine.typed.neuron;

public interface NeuronInterface {

	public static final int NEURON_CONNECT_FAILD = -1;
	public static final int NEURON_CONNECT_DONE = 1;
	public static final int NEURON_IN_IS_NULL = 2;
	public static final int NEURON_OUT_IS_NULL = 3;
	
	/**
	 * 
	 * @param 제공 받을 In객체
	 * @return {@code NeuronInterface.NEURON_CONNECT_FAILD} or {@code NeuronInterface.NEURON_CONNECT_DONE}
	 */
	public int out(Neuron nin);
	/**
	 * 
	 * @param 제공 받을 Out객체
	 * @return {@code NeuronInterface.NEURON_CONNECT_FAILD} or {@code NeuronInterface.NEURON_CONNECT_DONE}
	 */
	public int in(Neuron nout);
	
	
}
