package mine.typed.neuron;

import java.util.List;


/**
 * 정보 전달 기관중 정보를 외부로 즉 다른 뉴런으로 전달하는 기관 입니다.
 * 연결된 인풋 뉴런에 데이터를 전달 하기 전, 저장 기관의 데이터를 버퍼에 입력하고 버퍼를 인풋 뉴런에 전달합니다.
 * 전달된 버퍼는 삭제됩니다.
 * 중요한 것은 절대로 저장 기관의 정보를 직접 수정하는 일이 없도록 해야 한다는 것 입니다.
 * @author mrminer
 *
 */
public class NOut {

	public List<Object> tmpData;

	public static final byte CONNECT_IS_OFF = 0;
	public static final byte CONNECT_IS_DONE = 1;
	public byte connectState;

	public byte sendState;
	public static final byte SEND_DATA_TO_IN = 1;
	public static final byte DONT_SEND_DATA_TO_IN = 0;

	public void getData(NData ndata){
		this.tmpData = ndata.getData( );
	}

	/**
	 * NIn 과 연결합니다.
	 * In 의 connectIn() 도 같이 호출 해야 둘이 동시에 연결 됩니다.
	 * 그 이외의 연결 방법으론 수동으로 {@code connectState} 를 변경 해야 합니다.
	 * @param nin
	 * @return 성공 여부
	 */
	public boolean connectIn(NIn nin){
		this.connectState = NIn.CONNECT_IS_DONE;
		if((this.connectState == NIn.CONNECT_IS_OFF) || (nin.connectState == NOut.CONNECT_IS_OFF)){
			return false;
		}else{
			this.connectState = NIn.CONNECT_IS_DONE;
			nin.connectState = NOut.CONNECT_IS_DONE;
			if(nin.getState == NIn.DONT_GET_DATA_TO_OUT){

			}else{
				nin.tmpData = this.tmpData;
			}
			return true;
		}
	}

}
