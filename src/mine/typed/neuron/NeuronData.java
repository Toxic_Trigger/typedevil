package mine.typed.neuron;

import java.util.List;

import mine.typed.core.V2;

/**
 * 정보 근원지의 기본 구현체 입니다.
 * @author mrminer
 *
 */
public class NeuronData extends Neuron {

	/**
	 * 근원지의 생성
	 * @param 기본 데이터
	 * @param x
	 * @param y
	 */
	public NeuronData(List<Object> datas , float x, float y) {
		super(x, y);
		this.ndata.data = datas;
	}
	
	/**
	 * 근원지의 생성
	 * @param 기본 데이터
	 */
	public NeuronData(List<Object> datas) {
		super();
		this.ndata.data = datas;
	}
	
	/**
	 * 근원지의 생성
	 * @param 기본 데이터
	 * @param Pos
	 */
	public NeuronData(List<Object> datas, V2 Pos){
		super(Pos);
		this.ndata.data = datas;
	}

	@Override
	public int out(Neuron nin) {
		if(nin == null) return Neuron.NEURON_IN_IS_NULL;
		if(this.connectOutToIn(nin)) return Neuron.NEURON_CONNECT_DONE;
		return Neuron.NEURON_CONNECT_FAILD;
	}

	@Override
	public int in(Neuron nout) {
		if(nout == null) return Neuron.NEURON_OUT_IS_NULL;
		if(nin.connectOut(nout.nout)) return Neuron.NEURON_CONNECT_DONE;
		return Neuron.NEURON_CONNECT_FAILD;
	}
	
}
