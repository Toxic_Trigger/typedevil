package mine.typed.neuron;

import java.util.ArrayList;
import java.util.List;

/**
 * 뉴런이 정보를 전달 받는 기관으로, 오직 데이터의 수신만 가능합니다.
 * 데이터를 전달 받고 해당 데이터를 버퍼로 옮긴후, 저장 공간에 넣을지의 여부를 정하면
 * 저장 기관으로 버퍼의 데이터를 이동 시킵니다. 그와 동시에 버퍼의 데이터는 삭제 합니다.
 * 즉 모든 입력 기관은 1개의 데이터만을 반영구적인 소지가 가능합니다.
 * 영구적인 소지는 불가능 합니다.
 * 중요한 것은 절대로 저장 기관의 정보를 직접 수정하는 일이 없도록 해야 한다는 것 입니다.
 * @author mrminer
 *
 */
public class NIn{

	public List<Object> tmpData;

	public static final byte SEND_DATA_TO_NDATA = 1;
	public static final byte DONT_SEND_DATA_TO_NDATA = 0;
	public static final byte CONNECT_IS_OFF = 0;
	public static final byte CONNECT_IS_DONE = 1;

	public byte connectState;

	public byte state;

	public byte getState;
	public static final byte GET_DATA_TO_OUT = 1;
	public static final byte DONT_GET_DATA_TO_OUT = 0;

	public NIn(){
		this.tmpData = new ArrayList<Object>();
	}

	public boolean sendDataToNData(Neuron n){
		if(this.state == NIn.DONT_SEND_DATA_TO_NDATA){
			return false;
		}else{
			n.ndata.data = this.tmpData;
			return true;
		}
	}

	public void getData(NOut nout){
		this.tmpData = nout.tmpData;
	}

	/**
	 * Out 과 연결 합니다.
	 * Out 의 connectOut() 도 같이 호출 해야 둘이 동시에 연결 됩니다.
	 * 그 이외의 연결 방법으론 수동으로 {@code connectState} 를 변경 해야 합니다.
	 * @param nout
	 * @return 성공 여부
	 */
	public boolean connectOut(NOut nout){
		this.connectState = NIn.CONNECT_IS_DONE;
		if((this.connectState == NIn.CONNECT_IS_OFF) || (nout.connectState == NOut.CONNECT_IS_OFF)){
			return false;
		}else{
			this.connectState = NIn.CONNECT_IS_DONE;
			nout.connectState = NOut.CONNECT_IS_DONE;
			if(nout.sendState == NOut.DONT_SEND_DATA_TO_IN){

			}else{
				this.tmpData = nout.tmpData;
			}
			return true;
		}
	}


	public void cleanTmpData(){
		this.tmpData.clear( );
	}

}
