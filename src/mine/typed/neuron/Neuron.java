package mine.typed.neuron;

import mine.typed.core.V2;
import mine.typed.core.interfaces.Name;

/**
 * 뉴런의 기초적인 구현체 입니다.
 * 1개의 인풋 기관과 1개의 아웃 기관 그리고 1개의 정보 저장 공간이 있습니다.
 * 
 * 모든 뉴런의 어미 입니다.
 * @author mrminer
 *
 */
public abstract class Neuron implements Name , NeuronInterface{

	public final NIn nin;
	public final NOut nout;
	public NData ndata;

	private String name;

	public V2 pos;

	public final static byte CONNECTION_IS_DONE = 1;
	public final static byte CONNECTION_IS_END = 0;
	public final static byte CONNECTION_IS_FAILED = 2;
	public final static byte CONNECTION_IS_STAY = 3;
	public final static byte CONNECTION_IS_OFF = 4;

	public byte connectState;

	/**
	 * 기본 생성입니다.
	 * NData 1개 , NOut 1개 , NIn 1개를 가집니다.
	 * @param 뉴런이 만들어질 좌표
	 */
	public Neuron(V2 pos){
		this.ndata = new NData();
		this.nout = new NOut();
		this.nin = new NIn();
		this.pos = pos;
	}
	
	/**
	 * 위치를 가지지 않는 뉴런을 생성 합니다.
	 * NData 1개 , NOut 1개 , NIn 1개를 가집니다.
	 * @see this.pos = null 
	 */
	public Neuron(){
		this.ndata = new NData();
		this.nout = new NOut();
		this.nin = new NIn();
		this.pos = null;
	}

	/**
	 * 기본 생성입니다.
	 * NData 1개 , NOut 1개 , NIn 1개를 가집니다.
	 * @param 뉴런이 만들어질 좌표
	 */
	
	public Neuron(float x , float y){
		this.ndata = new NData();
		this.nout = new NOut();
		this.nin = new NIn();
		this.pos = new V2(x , y);
	}
	/**
	 * 기본 생성입니다.
	 * NData 1개 , NOut 1개 , NIn 1개를 가집니다.
	 * @param 뉴런이 만들어질 좌표
	 */
	
	public Neuron( V2 pos ,NOut Nout , NIn Nin ){
		this.ndata = new NData();
		this.nout = Nout;
		this.nin = Nin;
		this.pos = pos;
	}
	/**
	 * 기본 생성입니다.
	 * 
	 * NData 1개 , NOut 1개 , NIn 1개를 가집니다.
	 * @param 뉴런이 만들어질 좌표
	 */
	
	public Neuron( float x ,float y  ,NOut Nout , NIn Nin ){
		this.ndata = new NData();
		this.nout = Nout;
		this.nin = Nin;
		this.pos = new V2 (x , y);
	}

	/**
	 * 인자값의 뉴런의 인풋 기관에 자신의 아웃풋 기관을 연결 합니다.
	 * 이때 주의 해야 할 것은 해당 뉴런의 {@code connectState} 값이 {@code CONNECTION_IS_STAY} 이고 자신의 {@code connectState} 값이 {@code CONNECTION_IS_STAY} 일때만 연결에 성공 합니다.
	 * 인자 값 뉴런의 {@code connectState} 값이 {@code CONNECTION_IS_FAILED} 이라면 무조건 연결에 실패 합니다.
	 * 인자 값 뉴런의 {@code connectState} 값이 {@code CONNECTION_IS_OFF } 라면 시도 조차 하지 못합니다.
	 * @param n
	 * @return 성공 여부
	 */
	public boolean connectOutToIn(Neuron n){
		if((this.connectState == Neuron.CONNECTION_IS_STAY) && (n.connectState == Neuron.CONNECTION_IS_STAY)){
			this.connectState = Neuron.CONNECTION_IS_DONE;
			this.nin.connectOut( n.nout );
			return true;
		}else{
			return false;
		}
	}

	@Override
	public void setName( String name ) {
		this.name = name;
	}

	@Override
	public String getName( ) {
		return this.name;
	}

}
