package mine.typed.neuron;

import java.util.ArrayList;
import java.util.List;

/**
 * 저장 기관 입니다.
 * NIn 으로 전달된 정보를 보관하는 장소 입니다.
 * NIn 설명에 기술되어 있듯이 오로지 버퍼에 저장된 값이 승인 될때만 기록이 됩니다.
 * Nin , NOut 이 못하는 정보 수정을 NData 에선 가능 합니다.
 * @author mrminer
 *
 */
public class NData{

	public List<Object> data;

	public NData(){
		this.data = new ArrayList<Object>();
	}

	/**
	 * 저장된 데이터를 가져옵니다.
	 * @return
	 */
	public List<Object> getData(){
		return this.data;
	}

	/**
	 * 외부의 데이터를 저장합니다.
	 * @param objects
	 */
	public void setData(List<Object> objects ){
		this.data = objects;
	}
}
