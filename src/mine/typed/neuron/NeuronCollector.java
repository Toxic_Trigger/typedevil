package mine.typed.neuron;

import java.util.ArrayList;
import java.util.List;

/**
 * Neuron 객체를 수집하는 기능의 구현체<p>
 * <p>
 * 주로 수집된 Neuron 객체를 리스트로 관리 하며 수집된 객체에 개별적인 식별자를 붙여준다.
 * @author mrminer
 *
 */
public class NeuronCollector {
	private List<Neuron> neurons;

	public NeuronCollector(){
		this.initNeuronList( );
	}
	/**
	 * neurons 을 초기화 합니다.
	 */
	public void initNeuronList(){
		List<Neuron> tmp;
		tmp = new ArrayList<Neuron>();
		this.neurons = tmp;
	}

	/**
	 * Collector 에 인자값들을 추가 합니다.
	 * @param neuron
	 */
	public void add(Neuron ... neuron){
		final int len = neuron.length;
		for(int i = 0 ; i < len ; i++){
			final Neuron n = neuron[i];
			this.neurons.add( n );
		}
	}
	/**
	 * 인자값에 맞는 뉴런을 리스트에서 찾아서 리턴 합니다.
	 * @param  name 뉴런의 이름
	 * @return 이름이 일치하다면 일치되는 뉴런을 반환, 그렇지 않다면 null 을 리턴
	 */
	public Neuron getNeuron(String name) {
		final int len = this.neurons.size( );
		for(int i = 0 ; i < len ; i++){
			final Neuron tmp = this.neurons.get( i );
			if(tmp.getName( ).compareTo( name ) == 0) return tmp;
		}
		return null;
	}

}
