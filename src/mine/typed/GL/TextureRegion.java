
package mine.typed.GL;


/**
 * 텍스쳐에서 어떤 영역을 구분할때 쓰입니다.
 * @author mrminer
 *
 */
public class TextureRegion {
	public final float u1, v1;
	public final float u2, v2;
	public final Texture texture;

	/**
	 * texture 에서 x , y 를 사각형 좌측 상단 모서리로 정하고 width 와 height 값 만큼 우측과 하단 방향으로 사각형을 정의 합니다.
	 * @param texture
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 */
	public TextureRegion( final Texture texture, final float x, final float y, final float width,
			final float height ) {

		this.u1 = x / texture.width;
		this.v1 = y / texture.height;
		this.u2 = this.u1 + (width / texture.width);
		this.v2 = this.v1 + (height / texture.height);
		this.texture = texture;
	}
}
