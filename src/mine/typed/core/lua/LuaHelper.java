package mine.typed.core.lua;

public class LuaHelper {
	private static boolean isLuaUsable;
	private static boolean isLuaModeUsable;
	
	public static boolean hasLuaUsable() {
		return isLuaUsable;
	}
	public static void setLuaUsable(boolean isLuaUsable) {
		LuaHelper.isLuaUsable = isLuaUsable;
	}
	public static boolean hasLuaModeUsable() {
		return isLuaModeUsable;
	}
	public static void setLuaModeUsable(boolean isLuaModeUsable) {
		LuaHelper.isLuaModeUsable = isLuaModeUsable;
	}
}
