
package mine.typed.core.game;

import mine.typed.core.Rectangle;
import mine.typed.core.Sphere;
import mine.typed.core.V3;
import mine.typed.core.interfaces.Name;



public class GameObject3D  implements Name {
	public final V3 position;
	public final Sphere bounds;
	public final Rectangle mcb;

	private String Name;

	public static final String TYPE_GAME_OBJECT_3D = "<GameObject3D>";

	/**
	 * 3D 상에서의 오브젝트를 정의한다.
	 * 
	 * @param x
	 * @param y
	 * @param z
	 * @param radius
	 *            충돌체크
	 */
	public GameObject3D( final float x, final float y, final float z, final float radius ) {

		this.position = new V3( x , y , z );
		this.bounds = new Sphere( x , y , z , radius );
		this.mcb = null;
	}
	/**
	 * 3D 상에서의 오브젝트를 정의한다.
	 * 
	 * @param x
	 * @param y
	 * @param z
	 * @param cbW
	 *            충돌체크박스
	 * @param cbH
	 *            충돌체크박스
	 */
	public GameObject3D( final float x, final float y, final float z, final float cbW, final float cbH ) {

		this.position = new V3( x , y , z );
		this.bounds = null;
		this.mcb = new Rectangle( x - (cbW / 2) , y - (cbH / 2) , cbW , cbH );
	}

	@Override
	public int hashCode( ) {
		final int prime = 31;
		int result = 1;
		result = (prime * result) + ((this.Name == null) ? 0 : this.Name.hashCode( ));
		result = (prime * result) + ((this.bounds == null) ? 0 : this.bounds.hashCode( ));
		result = (prime * result) + ((this.mcb == null) ? 0 : this.mcb.hashCode( ));
		result = (prime * result) + ((this.position == null) ? 0 : this.position.hashCode( ));
		return result;
	}
	@Override
	public boolean equals( Object obj ) {
		if ( this == obj )
			return true;
		if ( obj == null )
			return false;
		if ( !(obj instanceof GameObject3D) )
			return false;
		final GameObject3D other = (GameObject3D) obj;
		if ( this.Name == null ) {
			if ( other.Name != null )
				return false;
		} else if ( !this.Name.equals( other.Name ) )
			return false;
		if ( this.bounds == null ) {
			if ( other.bounds != null )
				return false;
		} else if ( !this.bounds.equals( other.bounds ) )
			return false;
		if ( this.mcb == null ) {
			if ( other.mcb != null )
				return false;
		} else if ( !this.mcb.equals( other.mcb ) )
			return false;
		if ( this.position == null ) {
			if ( other.position != null )
				return false;
		} else if ( !this.position.equals( other.position ) )
			return false;
		return true;
	}
	@Override
	public void setName( String name ) {
		this.Name = name;
	}
	@Override
	public String getName( ) {
		return this.Name;
	}
	@Override
	public String toString( ) {
		return "GameObject3D [position=" + this.position + ", bounds=" + this.bounds + ", mcb=" + this.mcb + ", Name=" + this.Name + "]";
	}
}
