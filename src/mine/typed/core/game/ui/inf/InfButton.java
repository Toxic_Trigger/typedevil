package mine.typed.core.game.ui.inf;

public interface InfButton {
	
	public static final int BUTTON_DISABLED = 0;
	public static final int BUTTON_ACTIVATION = 1;
	
	/**
	 * 
	 * @return 활성화 , 비활성화 상태를 리턴
	 */
	public int getState();
	
	/**
	 * 활성화 , 비활성화 상태를 조정하는 함수.
	 * @param num
	 */
	public void setState(int num);
	
	/**
	 * 이름을 저장하는 변수는 반드시 private 제어자를 가지도록 하십시오.<p>
	 * Name 변수의 값을 인자값으로 하도록 구현 합니다.
	 * @param name
	 */
	public void setName(String name);
	/**
	 * 이름을 저장하는 변수는 반드시 private 제어자를 가지도록 하십시오.<p>
	 * @return Name 값을 반환 하도록 구현 합니다.
	 */
	public String getName();
	
	/**
	 *  인자값에 반응하는 코드를 만드세요.
	 * @param touch
	 */
	public void Event(boolean touch);
	
	/**
	 * 
	 * @return Touch 변수의 값을 리턴
	 */
	public boolean isTouch();
	
	/**
	 * 인자값으로 Touch 변수를 변경함.
	 * @param touch
	 */
	public void setTouch(boolean touch);
}
