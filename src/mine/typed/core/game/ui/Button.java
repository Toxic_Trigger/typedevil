package mine.typed.core.game.ui;

import mine.tools.debug.TypeDebug;
import mine.typed.GL.GLGame;
import mine.typed.core.V2;
import mine.typed.core.game.HitBox;
import mine.typed.core.game.ui.inf.InfButton;
import mine.typed.core.interfaces.Name;

/**
 * 버튼의 구현체
 * @author mrminer
 *
 */
public abstract  class Button extends HitBox  implements Name , InfButton{

	private String Name;

	public boolean isPush;
	public V2 pos;

	private int state;
	
	@Override
	public int getState(){
		return state;
	}
	
	@Override
	public void setState(int num){
		this.state = num;
	}
	

	/**
	 * 버튼을 생성 합니다.
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 */
	public Button( float x , float y , float width , float height ) {
		super( x , y , width , height );
		this.isAlive(GLGame.getDebug());
		this.pos = new V2(this.lowerLeft.x + (this.width / 2) ,this.lowerLeft.y + (this.height / 2));
	}
	/**
	 * 버튼을 생성 합니다.
	 * @param v
	 * @param width
	 * @param height
	 */
	public Button(V2 v , float width , float height ) {
		super( v.x ,v. y , width , height );
		this.isAlive(GLGame.getDebug());
		this.pos = new V2(this.lowerLeft.x + (this.width / 2) ,this.lowerLeft.y + (this.height / 2));
	}

	/**
	 * 해당 객체가 존재 하는지의 여부를 확인 합니다.
	 * @param arg0
	 */
	public void isAlive( TypeDebug arg0 ) {
		arg0.printMsg( this , "<Button>" , "is Gen" );
	}
	@Override
	public void setName( String name ) {
		this.Name = name;
	}
	@Override
	public String getName( ) {
		if(this.Name == null) return "null";
		return this.Name;
	}

	@Override
	public boolean isTouch( ) {
		return this.isPush;
	}
	
	@Override
	public void setTouch( boolean touch ) {
		this.isPush = touch;
	}

}
