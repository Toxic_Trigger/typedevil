package mine.typed.core.game.ui;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ButtonCollector {
	
	private List<Button> buttons;
	
	public ButtonCollector(){
		buttons = new ArrayList<Button>();
	}
	
	/**
	 * 버튼을 리스트에 등록 합니다.
	 * @param button
	 */
	public void registButton(Button button){
		buttons.add( button );
	}
	/**
	 * 
	 * @param Key 해당 인자 값으로 설정된 버튼을 찾습니다.
	 * @return 
	 */
	public Button getButton(int Key){
		Button tmp = buttons.get( Key );
		if( tmp == null){
			return null;
		}else{
			return tmp;
		}
	}
	
	/**
	 * 
	 * @param Name 검색하고자 하는 버튼의 이름
	 * @return 리스트에 있다면 해당 이름의 버튼을, 아니라면 null 을 리턴
	 */
	public Button getButton(String Name){

		Iterator<Button> tmpButtons = buttons.listIterator( );
		while(tmpButtons.hasNext( )){
			Button tmp = tmpButtons.next( );
			if(tmp.getName( ).matches( Name )){
				return tmp;
			}
		}
		return null;
	}
	
	
	/**
	 * 버튼 리스트를 반환 합니다.
	 * @return
	 */
	public List<Button> getButtons(){
		return buttons;
	}
	
}
