
package mine.typed.core.game;

import mine.typed.core.Circle;
import mine.typed.core.Rectangle;
import mine.typed.core.V2;
import mine.typed.core.interfaces.Name;

/**
 * 게임을 구성하는 객체들 중 움직임이 없는 객체들의 슈퍼 클래스 입니다.
 * @author mrminer
 *
 */
public   class GameObject  implements Name {
	public final V2 position;
	public final Rectangle bounds;
	public final Circle cir;

	private String Name;

	public static final String TYPE_GAME_OBJECT = "<GameObject>";




	/**
	 * 게임 객체를 생성 합니다. <p>
	 * 충돌 박스를 생성 합니다.
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 */
	public GameObject( final float x, final float y, final float width, final float height ) {

		this.position = new V2( x , y );
		this.bounds = new Rectangle( x - (width / 2) , y - (height / 2) , width , height );
		this.cir = null;

	}

	/**
	 * 게임 객체를 생성 합니다. <p>
	 * 충돌 박스를 생성 합니다.
	 * @param v
	 * @param width
	 * @param height
	 */
	public GameObject(V2 v, final float width, final float height ) {

		this.position = new V2( v.x , v.y );
		this.bounds = new Rectangle( v.x - (width / 2) , v.y - (height / 2) , width , height );
		this.cir = null;

	}

	/**
	 * 게임 객체를 생성 합니다. <p>
	 * 충돌 원을 생성 합니다.
	 * @param x
	 * @param y
	 * @param r 반지름 
	 */
	public GameObject( final float x, final float y, final float r ) {

		this.position = new V2( x , y );
		this.cir = new Circle( x , y , r );
		this.bounds = null;
	}

	/**
	 * 게임 객체를 생성 합니다<p>
	 * 충돌 원을 생성 합니다.
	 * @param v
	 * @param r
	 */
	public GameObject( V2 v , final float r ) {

		this.position = new V2( v.x , v.y );
		this.cir = new Circle( v.x , v.y , r );
		this.bounds = null;
	}

	@Override
	public int hashCode( ) {
		final int prime = 31;
		int result = 1;
		result = (prime * result) + ((this.Name == null) ? 0 : this.Name.hashCode( ));
		result = (prime * result) + ((this.bounds == null) ? 0 : this.bounds.hashCode( ));
		result = (prime * result) + ((this.cir == null) ? 0 : this.cir.hashCode( ));
		result = (prime * result) + ((this.position == null) ? 0 : this.position.hashCode( ));
		return result;
	}



	@Override
	public boolean equals( Object obj ) {
		if ( this == obj )
			return true;
		if ( obj == null )
			return false;
		if ( !(obj instanceof GameObject) )
			return false;
		final GameObject other = (GameObject) obj;
		if ( this.Name == null ) {
			if ( other.Name != null )
				return false;
		} else if ( !this.Name.equals( other.Name ) )
			return false;
		if ( this.bounds == null ) {
			if ( other.bounds != null )
				return false;
		} else if ( !this.bounds.equals( other.bounds ) )
			return false;
		if ( this.cir == null ) {
			if ( other.cir != null )
				return false;
		} else if ( !this.cir.equals( other.cir ) )
			return false;
		if ( this.position == null ) {
			if ( other.position != null )
				return false;
		} else if ( !this.position.equals( other.position ) )
			return false;
		return true;
	}

	@Override
	public void setName( String name ) {
		this.Name = name;
	}

	@Override
	public String getName( ) {
		return this.Name;
	}

	@Override
	public String toString( ) {
		return "GameObject [position=" + this.position + ", bounds=" + this.bounds + ", cir=" + this.cir + ", Name=" + this.Name + "]";
	}
}
