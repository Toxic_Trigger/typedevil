
package mine.typed.core.game;

import mine.tools.debug.TypeDebug;
import mine.typed.core.V2;



/**
 * 움직임이 가능한 게임 객체의 구현체 입니다.
 * @author mrminer
 *
 */
public class DynamicGameObject extends GameObject {
	public final V2 velocity;
	public final V2 accel;

	public HitBox hitbox;


	public static final String TYPE_DYNAMIC_GAME_OBJECT = "<DynamicGameObject>";

	/**
	 * 유동적인 오브젝트를 생성 합니다.
	 * 
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 */
	public DynamicGameObject( final float x, final float y, final float width, final float height ) {

		super( x , y , width , height );
		this.velocity = new V2( );
		this.accel = new V2( );
		this.hitbox = new HitBox( x - (width / 2) , y - (height / 2) , width , height );

	}

	/**
	 * 유동적인 오브젝트를 생성 합니다.
	 * @param v
	 * @param width
	 * @param height
	 */
	public DynamicGameObject( V2 v , final float width, final float height ) {

		super( v , width , height );
		this.velocity = new V2( );
		this.accel = new V2( );
		this.hitbox = new HitBox( v.x - (width / 2) , v.y - (height / 2) , width , height );

	}

	/**
	 * 충돌 박스가 좌표에 맞도록 수시로 업데이트 합니다.
	 */
	public void updateHitBox(){
		this.hitbox.lowerLeft.set( this.position.x - (this.bounds.width / 2) , this.position.y - (this.bounds.height / 2) );
	}


	/**
	 * 충돌 체크가 원 인 객체를 생성 합니다.
	 * 
	 * @param x
	 * @param y
	 * @param r
	 */
	public DynamicGameObject( final float x, final float y, final float r ) {

		super( x , y , r );
		this.velocity = new V2( );
		this.accel = new V2( );
	}

	/**
	 * 충돌 체크가 원 인 객체를 생성 합니다.
	 * @param v
	 * @param r
	 */
	public DynamicGameObject( V2 v, final float r ) {

		super( v , r );
		this.velocity = new V2( );
		this.accel = new V2( );
	}

	public  void isAlive(final TypeDebug da ) {
		if(da.isUseDebugger( )){
			da.printMsg( this , DynamicGameObject.TYPE_DYNAMIC_GAME_OBJECT , "X = " + this.position.x + "\nY = " + this.position.y );
		}else{

		}
	}

	@Override
	public int hashCode( ) {
		final int prime = 31;
		int result = super.hashCode( );
		result = (prime * result) + ((this.accel == null) ? 0 : this.accel.hashCode( ));
		result = (prime * result) + ((this.hitbox == null) ? 0 : this.hitbox.hashCode( ));
		result = (prime * result) + ((this.velocity == null) ? 0 : this.velocity.hashCode( ));
		return result;
	}

	@Override
	public boolean equals( Object obj ) {
		if ( this == obj )
			return true;
		if ( !super.equals( obj ) )
			return false;
		if ( !(obj instanceof DynamicGameObject) )
			return false;
		final DynamicGameObject other = (DynamicGameObject) obj;
		if ( this.accel == null ) {
			if ( other.accel != null )
				return false;
		} else if ( !this.accel.equals( other.accel ) )
			return false;
		if ( this.hitbox == null ) {
			if ( other.hitbox != null )
				return false;
		} else if ( !this.hitbox.equals( other.hitbox ) )
			return false;
		if ( this.velocity == null ) {
			if ( other.velocity != null )
				return false;
		} else if ( !this.velocity.equals( other.velocity ) )
			return false;
		return true;
	}

	@Override
	public String toString( ) {
		return "DynamicGameObject [velocity=" + this.velocity + ", accel=" + this.accel + ", hitbox=" + this.hitbox + "]";
	}


}
