package mine.typed.core.interfaces;

/**
 * 객체가 이름을 가지도록 합니다.<p>
 * 이름을 저장하는 변수는 반드시 private 제어자를 가지도록 하십시오.
 * @author mrminer
 *
 */
public interface Name {
	/**
	 * 이름을 저장하는 변수는 반드시 private 제어자를 가지도록 하십시오.<p>
	 * Name 변수의 값을 인자값으로 하도록 구현 합니다.
	 * @param name
	 */
	public void setName(String name);
	/**
	 * 이름을 저장하는 변수는 반드시 private 제어자를 가지도록 하십시오.<p>
	 * @return Name 값을 반환 하도록 구현 합니다.
	 */
	public String getName();
}
