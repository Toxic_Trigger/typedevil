package mine.typed.core;

import mine.typed.core.interfaces.Name;

/**
 * 큐를 구현 한 클래스 입니다. <p>
 * 
 * @author mrminer
 *
 */
public class QNode  implements Name {

	@Override
	public int hashCode( ) {
		final int prime = 31;
		int result = 1;
		result = (prime * result) + ((this.Name == null) ? 0 : this.Name.hashCode( ));
		result = (prime * result) + this.mInt;
		result = (prime * result) + ((this.mObject == null) ? 0 : this.mObject.hashCode( ));
		result = (prime * result) + ((this.mString == null) ? 0 : this.mString.hashCode( ));
		result = (prime * result) + ((this.next == null) ? 0 : this.next.hashCode( ));
		return result;
	}

	@Override
	public boolean equals( Object obj ) {
		if ( this == obj )
			return true;
		if ( obj == null )
			return false;
		if ( !(obj instanceof QNode) )
			return false;
		final QNode other = (QNode) obj;
		if ( this.Name == null ) {
			if ( other.Name != null )
				return false;
		} else if ( !this.Name.equals( other.Name ) )
			return false;
		if ( this.mInt != other.mInt )
			return false;
		if ( this.mObject == null ) {
			if ( other.mObject != null )
				return false;
		} else if ( !this.mObject.equals( other.mObject ) )
			return false;
		if ( this.mString == null ) {
			if ( other.mString != null )
				return false;
		} else if ( !this.mString.equals( other.mString ) )
			return false;
		if ( this.next == null ) {
			if ( other.next != null )
				return false;
		} else if ( !this.next.equals( other.next ) )
			return false;
		return true;
	}

	private String Name;

	protected final Object mObject;
	protected int mInt;
	protected String mString;

	protected QNode next;

	/**
	 * 큐 노드를 생성 합니다.
	 * @param 해당 노드가 가질 데이터
	 */
	public QNode(Object obj){
		this.mObject = obj;
		this.next = null;
	}

	/**
	 * 인자값을 가지는 큐 노드를 생성 합니다.
	 * @param var
	 */
	public QNode(int var){
		this.mObject = var;
		this.mInt = var;
		this.next = null;
	}

	/**
	 * 인자값을 가지는 큐 노드를 생성 합니다.
	 * @param str
	 */
	public QNode(String str){
		this.mObject = str;
		this.mString = str;
		this.next = null;
	}

	/**
	 * 다음 큐 노드를 설정 합니다.
	 * @param 다음 큐 노드
	 */
	public void setNextQNode( QNode qnode ){
		this.next = qnode;
	}
	/**
	 * 다음 큐 노드를 가져옵니다.
	 * @return 다음 큐 노드
	 */
	public QNode getNextQNode(){
		return this.next;
	}

	/**
	 * 데이터를 가져옵니다.
	 * @return 데이터
	 */
	public Object getObject(){
		return this.mObject;
	}

	@Override
	public void setName( String name ) {
		this.Name = name;
	}

	@Override
	public String getName( ) {
		return this.Name;
	}

	@Override
	public String toString( ) {
		return "QNode [Name=" + this.Name + ", mObject=" + this.mObject + ", next=" + this.next + "]";
	}

}
